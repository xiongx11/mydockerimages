-- This creates the database with inital data

create database phone;

use phone;

create table numbers (
  name varchar(50),
  number varchar(30)
);

insert into numbers values('Steve Shiling', '555-1234');
insert into numbers values('Will', '555-9999');
insert into numbers values('Augie', '555-4321');
